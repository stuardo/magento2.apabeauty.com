# note: this should never truly be refernced since we are using relative assets
css_dir         = "../css"
sass_dir        = "../scss"
images_dir      = "../images"
javascripts_dir = "../js"
relative_assets = true

add_import_path "../../default/scss/"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
# output_style = :expanded

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
# line_comments = true
# sourcemap = true


# LIVE
# environment = :production
# output_style = :compressed
# line_comments = false
# sourcemap = false

# DEV
environment = :development
output_style = :expanded
line_comments = true
sourcemap = true