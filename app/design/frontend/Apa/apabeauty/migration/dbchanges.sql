-- Fix errors in content page
UPDATE cms_page SET content = '
<script>
require(["jquery", "magento1/frontend/apa/default/js/slick.min"], function($, slick){
$("#homeSlider").slick({
        autoplay: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

});
</script>

<div id="homeSlider">
    <div>
        <a href="{{store url="apa-radiance-care.html"}}">
            <div id="para2" style="text-align:center;">
                <div class="center">
                    RADIANCE CARE
                    <h2 class="post">LIP LOOFAH | TOOTH GLOSS | BLUE LIP SHINE</h2>
                </div>
            </div>
        </a>
    </div>
    <div>
        <a href="{{store url="apa-white-duo.html"}}">
            <div id="para1">
                <div class="left">
                    <h2 class="post desk">WHITE FILMS | WHITE PEN</h2>
                </div>
                <div class="right" style="font-size:48px">
                    BREAKTHROUGH<br/>
                    IN HOME<br/>
                    WHITENING
                    <h2 class="post mob">WHITE FILMS | WHITE PEN</h2>
                </div>
            </div>
        </a>
    </div>
    <div>
        <div class="videoWrapper">
            <iframe src="//player.vimeo.com/video/235370528?autoplay=1&loop=1&title=0&byline=0&portrait=0" width="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>

' WHERE page_id = 2;
