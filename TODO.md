# After migration theme issues

- [x] products list: should show just the image + product name, no review, nor price under it
- [x] category page: should not show left sidebar
- [x] contact page: just phone and email
- [x] single page checkout
    Current solition is a single page checkout in 2 steps:
        1) fill in all details in one page
        2) confirmation page
    - https://amasty.com/one-step-checkout-for-magento-2.html
    - https://ecommerce.aheadworks.com/magento-2-extensions/one-step-checkout
    - https://www.mageplaza.com/magento-2-one-step-checkout-extension/#section-pricing
- [x] homepage 2 boxes should be carousel.
- [x] hompage, first slider, then video
- [x] products list, remove fav+compare+cart buttons
- [x] products list, remove hover shadow
- [x] Footer full lockup (Value proposition) + additional links
- [x] header, center logo
- [x] header, align top links + remove gray bar
- [x] Use the official fonts
- [x] Top Header: Adjust the welcome, Sign in & Create an Account
- [x] Header Nav: Add 25% more space in between.
- [x] The Slider: Make it bleed (full width)
- [x] The Slider: Add Vimeo video: (link to send)
- [x] Make the Apa Beauty Footer text 10px
- [x] Shop All: Display ALL products
- [ ] Product Single: remove the wishlist and Compare. Accordion
- [ ] press page, find a way to migrate the content

## General theme

- [x] links color to light-aqua color

## header

- [x] logo does not redirect to homepage
- [x] top menu hover menu light-aqua color
- [x] black header does not expando on wider screens
- [x] top search icons are not aligned
- [ ] Link to about/the-story is broken (redirectionm to the about page in the original site)
- [ ] First level link "shop¨ is broken
- [x] menu on active page, shows red left border

## footer

- [x] footer subscribe button is taller than the fieldbox
- [x] footer email subscription field redirects to "apa-white-duo"
- [x] footer email subscription icon is not aligned
- [ ] footer links are missing

##  pages

    ### content pages
        - [ ] content page has title and breadcrumbs navigation
        - [ ] homepage is broken
        - [ ] content columns are broken

    ### products list
        - [x] top page size dropdown shows cut
        - [x] bottom pae size dropdown is broken

    ### product detail page
        - [ ] error 404
        - [ ] ingredients not shown
        - [ ] details and more information have the same content

    ### checkout
        - [x] checkout page does not have the default style
        - [ ] checkout page needs spacing after the black header.
        - [ ] checkout page / unneded extra login on top right corner

## technicals

Technical issues can't be "seen" but are needed to make the project easier to maintain

- [x] loading leacy styles
- [x] migrate compass to existing theme
- [x] jquery is not loaded yet when trying to add slick carousel library
- [x] slick carousel is broken
- [ ] moving CSS from the DB to the theme (partial)